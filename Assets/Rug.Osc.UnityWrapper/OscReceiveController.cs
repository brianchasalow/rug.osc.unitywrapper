using System;
using System.Net;
using System.Collections;
using UnityEngine;
using Rug.Osc;
using System.Collections.Generic;
using System.Linq;

public class OscReceiveController : MonoBehaviour {



	[NonSerialized]
	public static Dictionary<string, OscReceiveController>oscReceiveControllers = new Dictionary<string, OscReceiveController>();

	#region Private Members
	// Receiver Instance 
	private OscReceiver m_Receiver;

	// Namespace manager instance
	private OscNamespaceManager m_Manager = new OscNamespaceManager(); 
	#endregion

	public bool useMulticast = false;
	public string multicastAddress = "";
	public int ListenPort = 5001;
	public int MaxMessagesToProcessPerFrame = 10;
	public OscNamespaceManager Manager { get { return m_Manager; } }
	private const int maxConnectRetries = 5;
	private int currConnectRetries = 0;

	private void Awake(){
		if(!oscReceiveControllers.ContainsKey(gameObject.name)){
			oscReceiveControllers.Add (gameObject.name, this);
		}
		else{
			string lastGOName = gameObject.name;
			gameObject.name = gameObject.name + UnityEngine.Random.Range(0, int.MaxValue);
			Debug.LogError ("Please give OscReceiveController GameObjects unique names! renaming " + lastGOName + " to: " + gameObject.name);
			oscReceiveControllers.Add (gameObject.name, this);
		}
	}

	public static OscReceiveController InstanceWithName(string name) {
			if(oscReceiveControllers.Count == 0){
				GameObject newOscReceiveCtrl = new GameObject();
				newOscReceiveCtrl.name = "_OscReceiveController" + UnityEngine.Random.Range(0, int.MaxValue);
				return newOscReceiveCtrl.AddComponent<OscReceiveController>();
			}

			if(oscReceiveControllers.ContainsKey (name)){
				return oscReceiveControllers[name];
			}
			else{
				string key = oscReceiveControllers.Keys.Last();
				return oscReceiveControllers[key];
			}
	}

	// Use this for initialization
	private void Start() {
		BeginConnect();
	}

	public void BeginConnect(){
		currConnectRetries = 0;
		Connect();
	}

	private void Connect(){
		// Ensure that the receiver is disconnected
		Disconnect (); 		

		if(currConnectRetries == maxConnectRetries){
			Debug.LogError("OscReceiveController: max retries connecting- connecting with dynamic port.");
			currConnectRetries++;
			ListenPort = 0;
		}
		else if(currConnectRetries > maxConnectRetries){
			Debug.LogError("OscReceiveController: no more retries. could not connect. network issues?");
			return;
		}


		currConnectRetries++;
		// The address to listen on to 
		IPAddress address = IPAddress.Any; 		
		// The port to listen on 
		int port = ListenPort;	

		IPAddress multiAddress;

		//if we could parse the IP, the ip is a multi address, and multicast is actually enabled
		if(IPAddress.TryParse(multicastAddress, out multiAddress) && OscSocket.IsMulticastAddress(multiAddress) && useMulticast){
			m_Receiver = new OscReceiver(address, multiAddress,port);
		}
		else{
			m_Receiver = new OscReceiver(address,port);
		}
		// Connect the receiver
		try{
			m_Receiver.Connect ();
			if(ListenPort == 0){
				ListenPort = m_Receiver.LocalPort;
			}
			// We are now connected
//			Debug.Log ("Connected Receiver"); 
		}
		catch (System.Net.Sockets.SocketException e) {
			if(e.SocketErrorCode == System.Net.Sockets.SocketError.AddressAlreadyInUse){
				Debug.LogError("Exception on OSC start! port " + ListenPort + " already bound. Trying again at port '"+ (ListenPort+1) +"'... error: " + e.ToString ());
				ListenPort++;
				Connect();
			}
		}

	}

	// Update is called once per frame
	void Update () {

		#if UNITY_EDITOR
		//prevent leaks in editor when editing/saving code, by forcing play mode to stop when compiling.
		if(UnityEditor.EditorApplication.isCompiling) {
			UnityEditor.EditorApplication.isPlaying = false;
		}
		#endif

		int i = 0; 

		// if we are in a state to recieve
		while (i++ < MaxMessagesToProcessPerFrame && 
		       m_Receiver.State == OscSocketState.Connected)
		{
			OscPacket packet;
		
			// get the next message this will not block
			if (m_Receiver.TryReceive(out packet) == false) 
			{
				return; 
			}
			
			switch (m_Manager.ShouldInvoke(packet))
			{
				case OscPacketInvokeAction.Invoke:
					// Debug.Log ("Received packet");
					m_Manager.Invoke(packet);
					break;
				case OscPacketInvokeAction.DontInvoke:
					Debug.LogWarning ("Cannot invoke");
					Debug.LogWarning (packet.ToString()); 
					break;
				case OscPacketInvokeAction.HasError:
					Debug.LogError ("Error reading osc packet, " + packet.Error);
					Debug.LogError (packet.ErrorMessage);
					break;
				case OscPacketInvokeAction.Pospone:
					Debug.Log ("Posponed bundle");
					Debug.Log (packet.ToString()); 
					break;
				default:
					break;
			}											
		}
	}	

	public void RegisterReceiveMethod(string address, OscMessageEvent msgEvent){
		Manager.Attach(address, msgEvent);
	}

	public void UnregisterReceiveMethod(string address, OscMessageEvent msgEvent){
		Manager.Detach(address, msgEvent);
	}

	// OnDestroy is called when the object is destroyed
	public void OnDestroy () {
		if(oscReceiveControllers.ContainsKey(gameObject.name))
			oscReceiveControllers.Remove(gameObject.name);

		Disconnect (); 
		m_Manager.Dispose (); 
	}
	
	private void Disconnect () {		
		// If the receiver exists
		if (m_Receiver != null) {			
			// Disconnect the receiver
//			Debug.Log ("Disconnecting Receiver"); 			
			m_Receiver.Dispose (); 			
			// Nullifiy the receiver 
			m_Receiver = null;
		}
	}
}
