using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rug.Osc;
using System.Linq;

public class OscSendController : MonoBehaviour {


	[System.NonSerialized]
	public static Dictionary<string, OscSendController>oscSendControllers = new Dictionary<string, OscSendController>();

	#region Private Members
	// Sender Instance 
	private OscSender m_Sender;
	#endregion
	
	public int RemotePort = 5000;	
	public string RemoteAddress = "127.0.0.1"; 
	public OscSender Sender { get { return m_Sender; } }

	private void Awake(){
		if(!oscSendControllers.ContainsKey(gameObject.name)){
			oscSendControllers.Add (gameObject.name, this);
		}
		else{
			string lastGOName = gameObject.name;
			gameObject.name = gameObject.name + UnityEngine.Random.Range(0, int.MaxValue);
			Debug.LogError ("Please give OSCSendController GameObjects unique names! renaming " + lastGOName + " to: " + gameObject.name);
			oscSendControllers.Add (gameObject.name, this);
		}
	}

	public void Send(OscMessage msg){
		if(Sender.State == OscSocketState.Connected){
			Sender.Send (msg);
		}
	}
	
	public static OscSendController InstanceWithName(string name) {
		if(oscSendControllers.Count == 0){
			GameObject newOscSendCtrl = new GameObject();
			newOscSendCtrl.name = "_OscSendController" + UnityEngine.Random.Range(0, int.MaxValue);
			return newOscSendCtrl.AddComponent<OscSendController>();
		}
		
		if(oscSendControllers.ContainsKey (name)){
			return oscSendControllers[name];
		}
		else{
			string key = oscSendControllers.Keys.Last();
			return oscSendControllers[key];
		}
	}


	// Use this for initialization
	void Start () {
		// Ensure that the sender is disconnected
		Disconnect (); 
		// The address to send to 
		IPAddress addess = IPAddress.Parse (RemoteAddress); 
		// The port to send to 
		int port = RemotePort;
		// Create an instance of the sender
		m_Sender = new OscSender(addess, 0, port);
		// Connect the sender
		m_Sender.Connect ();
//		// We are now connected
//		Debug.Log ("Sender Connected"); 
	}
	

	// OnDestroy is called when the object is destroyed
	public void OnDestroy () {
		if(oscSendControllers.ContainsKey(gameObject.name))
			oscSendControllers.Remove(gameObject.name);

		Disconnect (); 
	}
	
	private void Disconnect () {
		
		// If the sender exists
		if (m_Sender != null) {
			
			// Disconnect the sender
//			Debug.Log ("Disconnecting Sender"); 
			
			m_Sender.Dispose (); 
			
			// Nullifiy the sender 
			m_Sender = null;
		}
	}
}
