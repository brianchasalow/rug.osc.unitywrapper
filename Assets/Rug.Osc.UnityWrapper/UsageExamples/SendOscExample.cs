﻿using UnityEngine;
using System.Collections;

public class SendOscExample : MonoBehaviour {

	private OscSendController m_SendController; 
	
	#if UNITY_EDITOR
	[InspectorNoteAttribute("OSC Sender", "use the GameObject name of the desired OSC sender")]
	public bool GUIDisplay;
	#endif
	public string controllerName = "OSC Send Controller"; 

	private Vector3 m_LastPosition; 

	void Start () {
		m_SendController = OscSendController.InstanceWithName(controllerName);
		controllerName = m_SendController.gameObject.name;

		m_LastPosition = this.gameObject.transform.position; 
	}
		
	// Update is called once per frame
	void Update () {	

		if (m_SendController != null) {
			m_SendController.Send(new Rug.Osc.OscMessage("/test", Random.Range (0.0f, 100.0f)));

			if (m_LastPosition != transform.position) 
			{
//				Debug.Log("Send position"); 


				m_SendController.Send(new Rug.Osc.OscMessage("/test/pos", transform.position.x, transform.position.y, transform.position.z));
			}

			m_LastPosition = transform.position; 
		}
	}
}
