﻿using System.Collections;
using UnityEngine;
using Rug.Osc;
using System.Reflection;
public class ReceiveOscExample : MonoBehaviour {


	private OscReceiveController m_ReceiveController; 
#if UNITY_EDITOR
	[InspectorNoteAttribute("OSC Receiver", "use the GameObject name of the desired OSC receiver")]
	public bool GUIDisplay;
#endif
	public string controllerName = "Osc Receive Controller"; 

	// Use this for initialization
	public void Start () {
		m_ReceiveController = OscReceiveController.InstanceWithName(controllerName);
		controllerName = m_ReceiveController.gameObject.name;

		m_ReceiveController.RegisterReceiveMethod("/vdmx/colorPlz", ReceiveColor); 
		m_ReceiveController.RegisterReceiveMethod("/test/pos", ReceivePosition); 
		m_ReceiveController.RegisterReceiveMethod("/test", gotSomething);
		m_ReceiveController.RegisterReceiveMethod("/vdmx/string", ReceiveString); 
		m_ReceiveController.RegisterReceiveMethod("/vdmx/disIsABool", ReceiveBool);
		m_ReceiveController.RegisterReceiveMethod("/vdmx/sendFloat", ReceiveFloat);
		m_ReceiveController.RegisterReceiveMethod("/vdmx/reloadLevel", ReloadLevel);

	}
	
	void OnDestroy () {
			m_ReceiveController.UnregisterReceiveMethod("/vdmx/colorPlz", ReceiveColor); 
			m_ReceiveController.UnregisterReceiveMethod("/test/pos", ReceivePosition);
			m_ReceiveController.UnregisterReceiveMethod("/test", gotSomething);
			m_ReceiveController.UnregisterReceiveMethod("/vdmx/string", ReceiveString);
			m_ReceiveController.UnregisterReceiveMethod("/vdmx/disIsABool", ReceiveBool);
			m_ReceiveController.UnregisterReceiveMethod("/vdmx/sendFloat", ReceiveFloat);
			m_ReceiveController.UnregisterReceiveMethod("/vdmx/reloadLevel", ReloadLevel);
	}
	public void ReloadLevel(OscMessage message){
		Debug.Log ("reloading level...");
		Application.LoadLevel(Application.loadedLevelName);
	}

	public void gotSomething(OscMessage msg){
		Debug.Log ("got something: " + msg[0]);
	}

	public void ReceiveString(OscMessage message){
		if(message[0] is string){
			Debug.Log ("string is " + message[0]);
		}
	}

	public void ReceiveFloat(OscMessage message){
		if(message[0] is float){
			Debug.Log ("float is " + message[0]);
		}
	}

	public void ReceiveInt(OscMessage message){
		if(message[0] is int){
			Debug.Log ("int is " + message[0]);
		}
	}

	public void ReceiveBool(OscMessage message){
		if(message[0] is bool){
			Debug.Log ("bool is " + message[0]);
		}
	}

	public void ReceiveColor (OscMessage message) {		
		if(message[0] is OscColor){
			OscColor col = (OscColor)message[0];
			Color32 color = new Color32((byte)col.R,(byte) col.G, (byte)col.B,(byte) col.A);
			Debug.Log ("color!" + color);
			renderer.material.color = color;
		}
	}

	public void ReceivePosition (OscMessage message) {
	
		Debug.Log("Receive position"); 	
		// get the position from the message arguments 
		float x = (float)message [0];
		float y = (float)message [1];
		float z = (float)message [2];
		
		// assign the transform position from the x, y, z
		this.gameObject.transform.position = new Vector3 (x + 5, y + 5, z); 
	}

}
