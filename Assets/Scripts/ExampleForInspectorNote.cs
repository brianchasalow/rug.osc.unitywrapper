using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleForInspectorNote : MonoBehaviour
{
	[InspectorNote("Usage", "Apply to some unused boolean, its Unity PropertyDrawer just displays this stuff")]
	public bool noteIntroduction;

	public float someFloat;
	public float anotherFloat = 5f;

	public bool aBoolean;

	[InspectorNote("Header Only")]
	public bool noteHeaderOnly;

	public int MoreStuff;
	public Transform usualUnityStuff;

	[InspectorNote("Internal", "I like doing \"Internal\" sections instead of HideInInspector now")]
	public bool noteInternal;

	public float yetMoreStuff;
	public int hoorayPropertyDrawer;
}