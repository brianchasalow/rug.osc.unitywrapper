﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomPropertyDrawer(typeof(InspectorNoteAttribute))]
public class DrawerInspectorNote : PropertyDrawer
{
	/// <summary>
	/// Draw our editor
	/// </summary>
	/// <param name="position"></param>
	/// <param name="property"></param>
	/// <param name="label"></param>
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		InspectorNoteAttribute note = attribute as InspectorNoteAttribute;

		// our header is always present
		Rect posLabel = position;
		posLabel.y += 13;
		posLabel.x -= 2;
		posLabel.height += 13;
		EditorGUI.LabelField(posLabel, note.header, EditorStyles.whiteLargeLabel);

		// do we have a message too?
		if (!string.IsNullOrEmpty(note.message))
		{
			Color color = GUI.color;
			Color faded = color;
			faded.a = 0.8f;

			Rect posExplain = posLabel;
			posExplain.y += 15;
			GUI.color = faded;
			EditorGUI.LabelField(posExplain, note.message, EditorStyles.whiteMiniLabel);
			GUI.color = color;
		}

		Rect posLine = position;
//		for(int i = 0; i < note.numberOfLines; i++){

			posLine.y += string.IsNullOrEmpty(note.message) ? 30 : 42;
//			posLine.x += 15;
			posLine.height = 2;
//		}
			GUI.Box(posLine, "");
	}

	/// <summary>
	/// Override height in case of error
	/// </summary>
	/// <param name="prop"></param>
	/// <param name="label"></param>
	/// <returns></returns>
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		InspectorNoteAttribute note = attribute as InspectorNoteAttribute;

		return string.IsNullOrEmpty(note.message) ? 38 : 50;
	}
}
